@extends('layouts.layout')

@section('title')
	Kapitar - Projects
@endsection

@section('content')
    <div class="projects">
        <h1>Projects</h1>
        <div class="project-item">
            <a href="link">
                <h2>Title</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae assumenda quaerat cupiditate suscipit minus eveniet, tempora vel voluptatem? Dolor obcaecati, expedita vero placeat hic nobis maiores nulla eveniet. Dolores, voluptates!</p>
            </a>
        </div>
    </div>
@endsection
