@section('navbar')
<div class="container">
    <ul>
        <li class="logo"><div class="white">K</div>a<div class="white">p</div>ita<div class="white">r</div></li>
        <li style="float:right"><a class="transition" href="/contact">Contact</a></li>
        <li style="float:right"><a class="transition" href="/about">About</a></li>
        <li style="float:right"><a class="transition" href="/projects">Projects</a></li>
        <li style="float:right"><a class="transition" class="active" href="/">Home</a></li>
    </ul>
</div>
