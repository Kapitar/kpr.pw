@extends('layouts.layout')

@section('title')
	Kapitar - Web Developer
@endsection

@section('content')
<div class="main">
    <div class="box">
        <h1>Kapitar</h1>
        <p>Programmer & Web Developer</p>
        <a href="https://osu.ppy.sh/users/14068952" target="_blank">osu!</a>
        <a href="https://steamcommunity.com/id/Kapitarmain/" target="_blank">Steam</a>
        <a href="https://www.twitch.tv/k4p1t4r" target="_blank">Twitch</a>
        <a href="https://gitlab.com/Kapitar" target="_blank">GitLab</a>
    </div>
</div>
@endsection
